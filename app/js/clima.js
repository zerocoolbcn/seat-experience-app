var baseYahooURL = "https://query.yahooapis.com/v1/public/yql?q="
var selectedCity = config.coord.lugar;
var placeholder = "";
var unit = "c"

function init() {

  getWoeid(selectedCity);

    $('#city').keypress(function(event) {
        if (event.which == 13) {
            selectedCity = $('#city').val();
            getWoeid(selectedCity);
            $('#city').blur();
        }
    });

    $('#btn').click(function() {
        if ($('#btn').html() == "F") {
            unit = "c";
        } else unit = "f";
        $('#btn').html(unit.toUpperCase());
        getWoeid(selectedCity);
    })

    $('#city').focus(function(event) {
        placeholder = $("#city").val();
        $("#city").val("");
    });

    $('#city').blur(function(event) {
        if ($("#city").val() == "") {
            $("#city").val(placeholder);
        }
    });
}

function getWoeid(city) {
    var woeidYQL = 'select woeid from geo.placefinder where text="' + city + '"&format=json';
    var jsonURL = baseYahooURL + woeidYQL;
    $.getJSON(jsonURL, woeidDownloaded);
}

function woeidDownloaded(data) {
    

    var woeid = null;
    if (data.query.count <= 0) {
        $('#city').val("No se pudo obtener el clima de esta zona");
        $('#deg').html("");
        setImage(999, $("#big")[0]);
        for (var i = 0; i <= 3; i++) {
            $('#forecast' + i).html("");
            setImage(999, $("#forecastimg" + i)[0]);
            $('#forecastdeg' + i).html("");
        }
        return;
    } else if (data.query.count == 1) {
        woeid = data.query.results.Result.woeid;
    } else {
        woeid = data.query.results.Result[0].woeid;
    }
    getWeatherInfo(woeid);
}

function getWeatherInfo(woeid) {
    var weatherYQL = 'select * from weather.forecast where woeid=' + woeid + ' and u = "' + unit + '" &format=json';
    var jsonURL = baseYahooURL + weatherYQL

    $.getJSON(jsonURL, weaterInfoDownloaded);
}

function weaterInfoDownloaded(data) {

 

    $('#city').val(data.query.results.channel.location.city);
    $('#deg').html(data.query.results.channel.item.condition.temp + "°" + unit.toUpperCase());
    setImage(data.query.results.channel.item.condition.code, $('#big')[0]);

    var arryDias = Array()
    var vientos = data.query.results.channel.wind;
    var horaSol = data.query.results.channel.astronomy;
    var hum = data.query.results.channel.atmosphere;
    
    $('.humedad_tree').html("Humedad: "+hum["humidity"]+"%");
    $('.humedad_tow').html("Visibilidad: "+hum["visibility"]+"%");
    $('.humedad_tree').html("Presión: "+hum["pressure"]);

    $('.sunrise').html(horaSol["sunrise"]);
    $('.sunset').html(horaSol["sunset"]);

    $('#chill').html("Sensación termica: "+vientos["chill"]+" ºC");
        $('#direccion').html("Dirección: "+vientos["direction"]+" º");
            $('#velocidadWind').html("Velocidad: "+vientos["speed"]+" km/h");


    arryDias[0] = "DOM"
    arryDias[1] = "JUE"
    arryDias[2] = "LUN"
    arryDias[3] = "MAR"
    arryDias[4] = "MIE"
    arryDias[5] = "VIER"
    arryDias[6] = "SAB"

     $('.sunmoon .sun-animation').css('width', '70%');
    $('.sun-symbol-path').css('-webkit-transform', 'rotateZ(27deg)');

    var indice;

    for (var i = 0; i <= 3; i++) {
        var fc = data.query.results.channel.item.forecast[i];

        if (fc.day == 'Sun') {
            indice = 0;
        } else if (fc.day == 'Thu') {
            indice = 1;
        } else if (fc.day == 'Mon') {
            indice = 2;
        } else if (fc.day == 'Tue') {
            indice = 3;
        } else if (fc.day == 'Wed') {
            indice = 4;
        } else if (fc.day == 'Fri') {
            indice = 5;
        } else {
            indice = 6;
        }
        $('#forecast' + i).html(arryDias[indice]);
        setImage(fc.code, $("#forecastimg" + i)[0]);
        $('#forecastdeg' + i).html((parseInt(fc.low) + parseInt(fc.high)) / 2 + " °" + unit.toUpperCase());
    }
}

function setImage(code, image) {
     $('.loader2').css('display', 'none')
    
    image.src = "img/icons/";
    switch (parseInt(code)) {
        case 0:
            image.src += "Tornado.svg"
            break;
        case 1:
            image.src += "Cloud-Lightning.svg"
            break;
        case 2:
            image.src += "Wind.svg"
            break;
        case 3:
            image.src += "Cloud-Lightning.svg"
            break;
        case 4:
            image.src += "Cloud-Lightning.svg"
            break;
        case 5:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 6:
            image.src += "Cloud-Rain-Alt.svg"
            break;
        case 7:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 8:
            image.src += "Cloud-Drizzle-Alt.svg"
            break;
        case 9:
            image.src += "Cloud-Drizzle-Alt.svg"
            break;
        case 10:
            image.src += "Cloud-Drizzle-Alt.svg"
            break;
        case 11:
            image.src += "Cloud-Drizzle-Alt.svg"
            break;
        case 12:
            image.src += "Cloud-Drizzle-Alt.svg"
            break;
        case 13:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 14:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 15:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 16:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 17:
            image.src += "Cloud-Hail-Alt.svg"
            break;
        case 18:
            image.src += "Cloud-Hail-Alt.svg"
            break;
        case 19:
            image.src += "Cloud-Hail-Alt.svg"
            break;
        case 20:
            image.src += "Cloud-Fog.svg"
            break;
        case 21:
            image.src += "Cloud-Fog.svg"
            break;
        case 22:
            image.src += "Cloud-Fog.svg"
            break;
        case 23:
            image.src += "Cloud-Fog.svg"
            break;
        case 24:
            image.src += "Wind.svg"
            break;
        case 25:
            image.src += "Thermometer-Zero"
            break;
        case 26:
            image.src += "Cloud.svg"
            break;
        case 27:
            image.src += "Cloud-Moon.svg"
            break;
        case 28:
            image.src += "Cloud.svg"
            break;
        case 29:
            image.src += "Cloud-Moon.svg"
            break;
        case 30:
            image.src += "Cloud-Sun.svg"
            break;
        case 31:
            image.src += "Moon.svg"
            break;
        case 32:
            image.src += "Sun.svg"
            break;
        case 33:
            image.src += "Moon.svg"
            break;
        case 34:
            image.src += "Sun.svg"
            break;
        case 35:
            image.src += "Cloud-Hail-Alt.svg"
            break;
        case 36:
            image.src += "Sun.svg"
            break;
        case 37:
            image.src += "Cloud-Lightning.svg"
            break;
        case 38:
            image.src += "Cloud-Lightning.svg"
            break;
        case 39:
            image.src += "Cloud-Lightning.svg"
            break;
        case 40:
            image.src += "Cloud-Rain.svg"
            break;
        case 41:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 42:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 43:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 44:
            image.src += "Cloud.svg"
            break;
        case 45:
            image.src += "Cloud-Lightning.svg"
            break;
        case 46:
            image.src += "Cloud-Snow-Alt.svg"
            break;
        case 47:
            image.src += "Cloud-Lightning.svg"
            break;
        case 3200:
            image.src += "Moon-New.svg"
            break;
        case 999:
            image.src += "Compass.svg"
            break;
        default:
            image.src += "Moon-New.svg"
            break;
    }
}