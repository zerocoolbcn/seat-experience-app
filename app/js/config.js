var config = {

    /* configuracion general */
    app:
    {

        titulo: "SEAT EXPERIENCE",
        lengua: false,
        ulrws: "http://ws.seatexperience.net/",
        urlSoap: "http://ws.seatexperience.net/client.php?",
        versionWS: "ws1",
        color: null,
        iduser:null,
        dinamic_Data:null,
        contador: 5,
        evento_default: "30",
        login:false,
        idiomaContent:null,
        grap:true,
        minutosInactividadTelemetria:5
    },
    permisos:
    {
 
        loginInvitado:"",
        selector:"",
        dinamicas:"",
        videos:"",
        tecnicas:"none",
        reservas:"none",
        waypoints:"",
        roadbooks:"none",
        prensa:"",
        clima:"",
        telemetria:"",
        telefonos:"",
        interes:"",
        pie:""
    },
    conexion:
    {

        tipo: null
    },
    intervalos:
    {
        intervaloGeolocalizacion: 300,
        intervaloTraking: 1000,
        intervaloVelocidad: 3000,
        intervaloGetcity: 10000,
        loaderEmpty: 1000,
        storage:{
          intervalo_a:null,
          intervalo_b:null,
          intervalo_c:null 
        }

    },
    coord:
    {
        lat:null,
        lon:null,
        lugar:"Barcelona",

    },
    traking:
    {
        velocidad:null,
        maxvelocidad:null,
        session:null,
        usuario:null,
        latitud:null,
        longitud:null,
        GPSignal:0,
        rumbo:null,
        fuerza:null,
        altitud:null,
        aceleracionX:null,
        aceleracionY:null,
        aceleracionZ:null,
        timeStart:null

    },
    idioma: 
    {

        es:{
            lang1:"Hola invitado!",

        },
        en:{

            lang1:"HI invitado!",

        }
    },
    google: {

        analytics: 'UA-63354014-1',
        urlmap: "http://maps.googleapis.com/maps/api/geocode"
    },
}

var data = {
    
    app:
    {
         get:'43',
         name:'app'
    }
}