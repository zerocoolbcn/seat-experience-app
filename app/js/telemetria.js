var settings = {
      debug: localStorage['settings.debug'] === 'true' || false,
      units: localStorage['settings.units'] || 'metric', // or 'imperial'
      geoQueryInterval: 1000 
    };

    var points = [];
    var velocidad = [];
    var geolocation;
    var senddata = false;
    var $val = $('#speed .val');
    var $valmax = $('#speed .valmax');
    var $units = $('#speed .units');
    var $lat = $('.lat');
    var $lon = $('.long');

    var canvasGforcePi = document.getElementById("canvasGforce").getContext("2d");
  


    var $gpsBars = $('#gps-bars');
    var $gpsMessage = $('#gps-message');

    var EARTH_RADIUS = 6378000;  // meters


    var storage = {

    	telemetria:
    	{
    		latitud:0,
    		longitud:0,
    		distancia:0,
    		velocidad:0,
    		aceleracion:0,
    	}
    }

var velocidades = [];

var tel = {


     geo: function() {
            navigator.compass.getCurrentHeading(tel.rumbo, tel.fail);
            navigator.accelerometer.getCurrentAcceleration(tel.aceleracion, tel.fail);
     },
     rumbo: function(heading){
        
        var rotation = 360 - heading.magneticHeading-25,
        rotateDeg = 'rotate(' + rotation + 'deg)';

      $('.fa-compass').css('-webkit-transform', rotateDeg);


        config.traking.rumbo = heading.magneticHeading;

        $(".rum").html(Math.round(heading.magneticHeading)+" º")

     },
     liveGPS: function(data){
          

        
     },
     inicializaGPS: function()
     {

              var d = new Date();
              config.traking.timeStart = d.getMinutes();

               navigator.geolocation.watchPosition(
                    function(position) {
                      tel.registrarPunto(position); 
                    
                      position.second = getSeconds();  
                    },
                    function() {
                      console.log("Error GPS")
                     
                    },
                    {
                      enableHighAccuracy: true,
                      timeout:1000,
                      maximumAge: settings.geoQueryInterval
                     
                    }
                );  

        $units.text('km/h');
        config.traking.maxvelocidad = 0;
        
     },
     radiales: function(degrees){
      
        return degrees * 3.1415926 / 180;

     },
     registrarPunto: function(position)
     {
        var d = new Date();
        var point = {
            time: new Date(),
            timestamp: (navigator.platform === 'iPhone' || navigator.platform === 'iPad') ?
                          position.timestamp / 1000 :
                          position.timestamp,
            lat: position.coords.latitude,
            lon: position.coords.longitude,
            accuracy: position.coords.accuracy,
            speed: position.coords.speed * 3600/ 1609
          };

          var co_lat = position.coords.latitude.toFixed(4);
          var co_lon = position.coords.longitude.toFixed(4);

           $lat.text(co_lat);
           $lon.text(co_lon);

           if(config.traking.latitud==co_lat)
           {
                
                var n = d.getMinutes();
               

                if(Math.abs(config.traking.timeStart-n)>config.app.minutosInactividadTelemetria)
                {
                  // Detenemos el traking por inactividad.              
                  clearInterval(config.intervalos.storage.intervalo_a);
                  clearInterval(config.intervalos.storage.intervalo_b);

                  $('.telemetriaStartStop').css('display', '');
                  $('.telemetria').css('display', '');
                  $('.telemetriaStart').css('display', '');
                  $('.BtonTelemetria').css('display', '');

                  tel.borrarTelemetriaFallida()
                }

            } else
            {
               config.traking.timeStart = d.getMinutes();
            }

          if(!(config.traking.latitud==co_lat) || !(config.traking.longitud==co_lon)){
           
            senddata = true;
          
          }else
          {
            senddata = false;
          }

           config.traking.latitud = co_lat;
           config.traking.longitud = co_lon;


          points.push(point);
   
          tel.ActualizaVelocidad();
          tel.GPSAccuracy(point);

     },

     borrarTelemetriaFallida: function(){

      var datos = "session="+config.traking.session

        $.ajax({
          type: 'post',
          url:  config.app.ulrws+"trakingDelete.php",
          data: datos
        });
        
        config.traking.velocidad = null;
        config.traking.maxvelocidad = null;
        config.traking.session = null;
        config.traking.usuario = null;
        config.traking.latitud = null;
        config.traking.longitud = null;
        config.traking.rumbo = null;
        config.traking.fuerza = null;
        config.traking.altitud = null;
        config.traking.aceleracionX = null;
        config.traking.aceleracionY = null;
        config.traking.aceleracionZ = null;
        config.traking.timeStart = null;

     },
     enviarData: function(){

        if(senddata){

            var datos = "velocidad="+config.traking.velocidad+'&maxvelocidad='+config.traking.maxvelocidad+'&session='+config.traking.session+'&latitud='+config.traking.latitud+'&longitud='+config.traking.longitud+'&rumbo='+config.traking.rumbo+'&fuerza='+config.traking.fuerza+'&altitud='+config.traking.altitud+'&aceleracionX='+config.traking.aceleracionX+'&aceleracionY='+config.traking.aceleracionY+'&aceleracionZ='+config.traking.aceleracionZ;

            if(config.traking.GPSignal>0){

                $.ajax({
                    type: 'post',
                    url: config.app.ulrws+"traking.php",
                    data: datos,
                    dataType: 'json'
                });

            }

        }
     },
     GPSAccuracy: function(point){
      
        if(point.accuracy > 25) {
            config.traking.GPSignal = 1;
            $gpsBars.addClass('one').removeClass('two').removeClass('three');
            $gpsMessage.text('Señal GPS mala');
          } else if (point.accuracy > 6) {
            $gpsBars.addClass('two').removeClass('one').removeClass('three');
            $gpsMessage.text('Señal GPS media');
            config.traking.GPSignal = 1;
          } else {
            $gpsBars.addClass('three').removeClass('one').removeClass('two');
            $gpsMessage.text('Señal GPS Buena');
            config.traking.GPSignal = 1;
          }     
     },
     distancias: function(point1, point2)
     {
          var lat1 = tel.radiales(point1.lat);
          var lat2 = tel.radiales(point2.lat);
          var lon1 = tel.radiales(point1.lon);
          var lon2 = tel.radiales(point2.lon);
          
          var x = (lon2 - lon1) * Math.cos((lat1 + lat2)/2);
          var y = lat2 - lat1;

          return Math.sqrt(x * x + y * y) * EARTH_RADIUS;
     },
     ActualizaVelocidad: function(){

        if (points.length > 1) {

            var p1 = points[points.length - 2];
            var p2 = points[points.length - 1];   

            var distance = tel.distancias(p2, p1);  

            console.log(distance)
            
            var time = (p2.timestamp - p1.timestamp) / 1000 / 60 / 60; 
            var divisor = settings.units === 'metric' ? 1000 : 1609;
            if (time > 0) {

              var speed = (distance / time) / divisor;
            
              if (speed < 5) {
                speed = speed.toFixed(1);
              } else {
                speed = Math.round(speed);
              }
              var veloc = Math.round(speed);

                    if(Math.abs(config.traking.velocidad - veloc)<40){

                      velocidades.push(veloc);
                    
                   }
            }
          }
         
     },
     ImprimirVelocidad: function()
     {
        cont = 0;
        total = 0;

        for(x=valores.length-1;x>=0;x--)
        {
         cont++; // contamos cuantos elementos hemos elegido
         total += valores[x]; // sumamos el total
         if(cont == 3) break;
        }

        var mediadeVelocidad = total/cont;

        $val.text(mediadeVelocidad);

        config.traking.velocidad = mediadeVelocidad;

        if(config.traking.maxvelocidad<config.traking.velocidad)
        {
                config.traking.maxvelocidad = config.traking.velocidad;
                $valmax.text(config.traking.maxvelocidad+" max");
        }

     },
     CreateCanvasGForce: function(x,y)
     {


        var xx = 260/2;
        var yy = 260/2;
                  
                xx =  Math.round(x)+xx;
                yy =  Math.round(y)+yy;



        canvasGforcePi.clearRect(0,0, 200,200);
        canvasGforcePi.beginPath();
        canvasGforcePi.fillStyle="#93C5DA";
            
        canvasGforcePi.arc(xx,yy,10,0,Math.PI*2,true);
        canvasGforcePi.closePath();
        canvasGforcePi.fill();


     },
     fail: function(error)
     {

     	 alert('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
     },
        /**
         * [aceleracion description]
         * @param  {[type]} aceleracion [description]
         * @return {[type]}             [description]
         */
     aceleracion: function(aceleracion){

 
  

        var acce = config.traking.velocidad / 1000;  


        var y = aceleracion.y;
        var x = aceleracion.x;
        var z = aceleracion.z;

        config.traking.aceleracionX =  parseFloat(aceleracion.x).toFixed(4);
        config.traking.aceleracionY = parseFloat(aceleracion.y).toFixed(4);
        config.traking.aceleracionZ = parseFloat(aceleracion.z).toFixed(4);


      

        tel.CreateCanvasGForce(x,y);

    
     },
     alertaGPS: function (mensaje)
     {
     	$(".alertaGPS").show();
     	$(".alertaMensajeGPS").html(mensaje);
     }

}

