    var evento = config.app.evento_default;
    var login = false;
    var debug = false;
    var currentNum;
    var vibracion = false;
    var varTimerIntervalGeo;
    var varTimerIntervalNet;
    var varTimerIntervalSendData;
    var varTimerIntervalGeo;
    var varTimerIntervalgetRumbo;
    var varTimerIntervalLogin = false;
    var varTimerIntervalCity = false;
    var falloConexion = false;
    var datosCargados = false;

    window.apikey = evento; //CryptoJS.MD5(evento);


    var idioma = function(jsdata) {

        $("[tkey]").each(function(index) {
            var strTr = jsdata[$(this).attr('tkey')];

            $(this).html(strTr);
        });

    }


    var app = {
        /**
         * [initialize description]
         * @return {[type]} [description]
         */
        initialize: function() {



            document.location = "#home";

            if (config.app.lengua == false) {


                $(".idm").click(function() {

                    var lang = $(this).attr('rel');

                    config.app.lengua = lang;



                    $.getJSON(config.app.ulrws + 'lang/' + config.app.lengua + '.php', function(data) {

                        idioma(data);
                        config.app.idiomaContent = data;

                        app.initialize();
                    });


                });


            } else {


                $("#idiomaSelectorInt").css("display", "none");
                $("#login").css("display", "");



                $("#dinamicas").css("display", config.permisos.dinamicas);
                $("#videos").css("display", config.permisos.videos);
                $("#tecnicas").css("display", config.permisos.tecnicas);
                $("#reservas").css("display", config.permisos.reservas);
                $("#prensa").css("display", config.permisos.prensa);
                $("#waypoints").css("display", config.permisos.waypoints);
                $("#roadbooks").css("display", config.permisos.roadbooks);
                $("#clima").css("display", config.permisos.clima);
                $("#telemetria").css("display", config.permisos.telemetria);
                $("#telefonos").css("display", config.permisos.telefonos);
                $("#interes").css("display", config.permisos.interes);

                $(".botonHash .menu").on("click", function() {

                    var rel = $(this).attr('rel');
                    var id = $(this).data("id");

                    data.app.get = id;

                    window.location.hash = rel;
                });


                $("#dinamicas").css("display", config.permisos.dinamicas);
                $("#videos").css("display", config.permisos.videos);

                $("#pie").css("display", config.permisos.pie);

                this.bindEvents();

            }

        },
        /**
         * [bindEvents description]
         * @return {[type]} [description]
         */

        bindEvents: function() {

            document.addEventListener("deviceready", this.onDeviceReady, false);
            app.conexxx();
        },
        /**
         * [onDeviceReady description]
         * @return {[type]} [description]
         */
        onDeviceReady: function() {

           // window.analytics.startTrackerWithId(config.google.)
            // ejecucion de las variables 
            app.conexxx();
            app.urlvacia();
            init();



        },
        jumpClima: function() {
            app.loader(false);
            app.resetDesktop();
            
            $('.clima').css('display', '');

            if( !(window.location.hash=='#clima') ){

                document.location = "#clima";
            }
        },
        /**
         * [fail description]
         * @return {[type]} [description]
         */
        fail: function() {


            console.log("a ocurrido un error");

        }, // funcion para la obtencion de clima de yahoo
        /**
         * [clima description]
         * @return {[type]} [description]
         */
        clima: function() {

        }, // funcion para comprobar si el hash de la url no este vacia 
        /**
         * [urlvacia description]
         * @return {[type]} [description]
         */
        urlvacia: function() {
            var jash = window.location.hash;

            if (jash == '' || jash == 'undefined') {
                document.location = '#home';
            }
        },
        /**
         * [efectoContador description]
         * @return {[type]} [description]
         */
        getCIty: function() {



            $.ajax({
                type: 'GET',
                dataType: "json",
                url: config.google.urlmap + "/json?latlng=" + config.coord.lat + "," + config.coord.lon + "&sensor=false",
                data: {},
                success: function(data) {
                    $('#city').html(data);
                    $.each(data['results'], function(i, val) {
                        $.each(val['address_components'], function(i, val) {
                            if (val['types'] == "locality,political") {
                                if (val['long_name'] != "") {

                                    config.coord.lugar = val['long_name'];
                                    init();
                                } else {
                                    config.coord.lugar = "Barcelona";
                                }

                            }
                        });
                    });

                },
                error: function() {
                    console.log('error');
                }
            });
        },
        efectoContador: function() {

            varTimerIntervalLogin = setInterval(function() {
                anim();
            }, 900);
            $('#countdown').css('display', '');

            currentNum = config.app.contador;
            $("#countdown").html(config.app.contador); // init first time based on n

        },
        /**
         * [lanzadorIntervalosPersistentes description]
         * @return {[type]} [description]
         */
        lanzadorIntervalos: function() {

            $('.telemetriaStart').css('display', 'none');
            $('.telemetriaStartMensaje').css('display', 'none');

            config.intervalos.storage.intervalo_a = setInterval(tel.geo, config.intervalos.intervaloGeolocalizacion);
            config.intervalos.storage.intervalo_b = setInterval(tel.enviarData, config.intervalos.intervaloTraking);
            config.intervalos.storage.intervalo_c = setInterval(tel.ImprimirVelocidad, config.intervalos.intervaloVelocidad);

        },
        /**
         * [login description]
         * @return {[type]} [description]
         */
        login: function() {

            var user = $("#username").val();
            var password = $("#password").val();


            window.apikey = CryptoJS.MD5(user + '%%' + CryptoJS.MD5(password));


            var url = config.app.ulrws + window.apikey + "/" + config.app.versionWS + "/" + config.app.lengua + "/get.login.null.json/0";


            $.getJSON(url, function(json) {




                if (json.status > 0) {

                    $('.BtonLoginSumit').html(json.nombre + ' ...');

                   // messg.success('¡Perfecto! ' + json.nombre + ', calentando motores...', 4000);

                   window.videoHome = json.videoBienvenida;

                    config.app.login = true;
                    config.traking.usuario = user;

                    $('#username').css('display', 'none');
                    $('#password').css('display', 'none');

                    $("#reservas").css("display", "");

                    $('#cargador').removeClass('loader');
                    $('#cargador').addClass('loaderMov');

                    app.efectoContador();

                    var timeSet = config.app.contador * 1000;

                    setTimeout(app.contructor, timeSet, json, true);
                    setTimeout(app.hideSplash, timeSet, json, true);

                } else {
                    
                        $(".BtonLoginSumit").click(function() {
                            app.login();
                            $(".BtonLogin").unbind();
                            $(".BtonLoginSumit").unbind();
                            
                        });
                    

                    messg.error('Los datos no son valido, compruébalos', 4000);

                }
            });
        },
        /**
         * [getDatosJson description]
         * @return {[type]} [description]
         */

        getDatosJson: function() {

            var url = config.app.ulrws + window.apikey + "/" + config.app.versionWS + "/" + config.app.lengua + "/get.event.null.json/0";
            $.getJSON(url, function(json) {
                if (json.status > 0) {
                    app.contructor(json, false);
                }
            });
        },
        /**
         * [contructor description]
         * @param  {[type]} json      [recoje datos en json del servidor]
         * @param  {[type]} loginView [variable boleana de login]
         * @return {[type]}           [description]
         */
        contructor: function(json, loginView) {
            
            datosCargados = true;


            var color = Array;
            color[0] = "#CCCC33";
            color[1] = "#FF6600";
            color[2] = "#0099CC";
            color[3] = "#FF66FF";

            var imageUrl = json.background;

            config.app.color = '#fff';

            $(".info").css('background-imag', '#fff');

            $(".telts").css('background-color', '#fff');
            $("#canvasGforce").css('background-color', '#C9272C');

            $(".identi").css('color', '#fff');


            $(".identi").css('color', '#fff');
        
            $(".contenidoCOnten").css('background-color', '#fff');

            //window.color = json.color;

            /**
             * [loginView variavle boleana que detecta si esta o no logeado]
             * @type {[type]}
             */
            if (loginView == false) {
                $('#nameUser').html(config.idioma.es.lang1);
                $('#reservas').css('display', config.permisos.reservas);
                $('.pieLOgin').css('display', '');
                $('.pie').css('display', 'none');
            } else {
                $('#reservas').css('display', config.permisos.reservas);
                $('.pie').css('display', '');
                $('.pieLOgin').css('display', 'none');
                $('#nameUser').html(json.nombre);
                $('#nameMedio').html(json.medio);
            }


            $('#nameEvent').html(json.evento);
            $('#descripcion').html(json.descripcion);
            $('#nameEvent').html(json.evento);
            $("#logo").attr("src", json.logoMedio);

            // contadores
            $('#contadorVideos').html(json.countVideos);
            $(".menucolor").css('border-bottom', '4px solid ' + '#fff');
        
            //ani // // // // // // // // // // // // // 
            $('#contadorDinamicas').html(json.countDinamicas);
            $('#contadorEstaticas').html(json.countEstaticas);
            $('#contadorCoches').html(json.countCoches);
            $('#contadorKML').html(json.countKML);
            $('#contadorway').html(json.countWAY);

            setTimeout(MostrarBotones, 3000);


        },
        /**
         * [conexxx funcion para comprobar si tenemos conexion a internet y que tipo de conexion]
         * @return {[type]} [null]
         */
        conexxx: function() {

            var networkState = navigator.connection.type;


            var states = {};

            states[Connection.UNKNOWN] = '';
            states[Connection.ETHERNET] = 'Conexión Ethernet';
            states[Connection.WIFI] = 'Conectado a WiFi <i class="fa fa-wifi"></i>';
            states[Connection.CELL_2G] = 'Conexión 2G';
            states[Connection.CELL_3G] = 'Conexión  3G';
            states[Connection.CELL_4G] = 'Conexión 4G';
            states[Connection.NONE] = 'Sin conexión';


            config.conexion.tipo = states[networkState];


            if (config.conexion.tipo == 'Sin conexion') {
                falloConexion = true;



            } else {

                $('#conexion').css('display', '');
                $('#noconexion').css('display', 'none');

                if (falloConexion) {
                    //restablecemos
                    if (datosCargados == false) {
                        app.getDatosJson();

                          
                        messg.error('Los datos no son valido, compruébalos', 1000);
                    }
                    falloConexion = false;
                }


                //  $(".version").html(config.conexion.tipo);
            }

        },
        /**
         * [changeGallery description]
         * @param  {[type]} id [description]
         * @return {[type]}    [description]
         */
        changeGallery: function(id) {

            if (id == 1) {
                $('.Divdinamicas').css('display', '');
                $('.Divestaticas').css('display', 'none');


            } else {

                $('.Divdinamicas').css('display', 'none');
                $('.Divestaticas').css('display', '');
            }
        },

        /**
         * [openFile description]
         * @param  {[type]} url [description]
         * @return {[type]}     [description]
         */
        openFile: function(url) {

            messg.info('Abriendo archivo remoto un momento...', 8500);

            var open = cordova.plugins.disusered.open;

            function success() {

            }

            function error(code) {
                if (code === 1) {
                    alert('Imposible abrir este archivo, actualmente. error 001');
                } else {
                    alert('Imposible abrir este archivo, actualmente. error 002');
                }
            }

            open(url, success, error);

        },
        /**
         * [nave description]
         * @param  {[type]} dir [description]
         * @return {[type]}     [description]
         */
        nave: function(dir) {

            navigator.google_navigate.navigate(dir, function() {

            }, function(errMsg) {
                console.log("Failed: " + errMsg);
            });

        },
        /**
         * [resetDesktop description]
         * @return {[type]} [description]
         */
        resetDesktop: function() {

            clearInterval(varTimerIntervalLogin);


            $('.wsContenido').empty();
            $('#home').css('display', 'none');
            $('.contenido').css('display', 'none');
            $('.telemetriaZone').css('display', 'none');
            $('.clima').css('display', 'none');
            $('.telemetria').css('display', 'none');
        },
        /**
         * [playerVideo description]
         * @param  {[type]} url [description]
         * @return {[type]}     [description]
         */
        playerVideo: function(url) {

            //VideoPlayer.play(url);
            console.log(url)
            messg.info('Iniciando reproduccion...', 3500);

            data.app.get = url;

            window.location.hash = 'reproductor';
        },
        /**
         * [download description]
         * @param  {[type]} url      [description]
         * @param  {[type]} fileName [description]
         * @return {[type]}          [description]
         */
        download: function(url, fileName) {

            messg.info('La descarga se ha iniciado...', 1500);

            var store = cordova.file.dataDirectory;
            var downloadUrl = url;
            var relativeFilePath = store + "/" + fileName;
            console.log(relativeFilePath);

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fileSystem) {
                var fileTransfer = new FileTransfer();
                fileTransfer.download(
                    downloadUrl,

                    // The correct path!
                    fileSystem.root.toURL() + '/' + relativeFilePath,

                    function(entry) {
                        messg.success('La descarga se ha completado', 2500);

                    },
                    function(error) {
                        messg.error('Un error inesperado impidió descargar este archivo, inténtalo de nuevo mas tarde', 3500);
                    }
                );
            });

            /*
                            fileTransfer.download(url, store + fileName, 
                                function(entry) {
                                    console.log("Success!");
                                   
                                }, 
                                function(err) {
                                    console.log("Error");
                                    console.dir(err);
                                });
                */
        },
        share: function(url) {

            messg.info('Iniciando compartir archivo...', 3500);
            window.plugins.socialsharing.share(null, null, url, null);
        },
        loader: function(bol) {


            if (bol) {
                $('.esperandoAnime').css('display', '');
                $('.esperando').css('display', '');
            } else {
                $('.esperandoAnime').css('display', 'none');
                $('.esperando').css('display', 'none');

            }


        },
        /**
         * [hideSplash description]
         * @return {[type]} [description]
         */
        hideSplash: function() {

            idioma(config.app.idiomaContent);
            //$('.splash').empty()
            $('.btn').css('display', 'none');
            $('.texloading').css('display', 'none');
            $('#idioma').animate({
                marginLeft: '-900px'
            }, 700);

        },
        /**
         * [receivedEvent description]
         * @param  {[type]} id [description]
         * @return {[type]}    [description]
         */
        receivedEvent: function(id) {

        }
    };

    function addClassDelayed(jqObj, c, to) {
        setTimeout(function() {
            jqObj.addClass(c);
        }, to);
    }

    function anim() {
        addClassDelayed($("#countdown"), "puffer", 600);
        if (currentNum == 0) {
            currentNum = config.app.contador - 1;
            clearInterval(varTimerIntervalLogin);
            $('#countdown').html(0);
        } else {
            currentNum--;
            $('#countdown').html(currentNum + 1);

            navigator.notification.vibrate(30);


            $('#countdown').removeClass("puffer");
        }

    }


    // sistema de navegacion por hash sin frameworks
    window.onpopstate = function(event) {
        //app.urlvacia();           
        clearInterval(config.intervalos.storage.intervalo_a);
        clearInterval(config.intervalos.storage.intervalo_b);
        clearInterval(config.intervalos.storage.intervalo_c);
        
        $('.wsContenido').empty();

        $('html,body').scrollTop(0);

        if (window.menuEstado) {
            $('#menu').css('display', 'none');
     
            window.menuEstado = false;
        }

        $('.clima').css('display', 'none')
        $('.telemetria').css('display', 'none')

        varTimerIntervalLogin = false;

        $('.atras').css('display', '');

        var jash = window.location.hash;

        if (jash == '#home') {
            app.resetDesktop();
            $('#home').css('display', '');
            $('.atras').css('display', 'none');

        } else if (jash == '#clima') {

            

            app.resetDesktop()
            $('.clima').css('display', '');
            
            init();

            if( !(window.location.hash=='#clima') ){

                document.location = "#clima";
            }

        } else if (jash == '#telemetria') {

            app.resetDesktop();
            app.loader(true);

            if( !(window.location.hash=='#telemetria') ){

                document.location = "#telemetria";
            }

            var options = {
                maximumAge: 0,
                timeout: 5500,
                enableHighAccuracy: true
            };
            navigator.geolocation.getCurrentPosition(function() {

                app.loader(false)


                $('.telemetriaStartMensaje').css('display', '');
                $('.telemetria').css('display', '');
                $('.telemetriaStart').css('display', '');
                $('.BtonTelemetria').css('display', '');

                $(".BtonTelemetria").click(function() {

                    $('.BtonTelemetria').css('display', 'none');
                    app.lanzadorIntervalos();
                    config.traking.session = uniqid();

                    var velocidades = [];

                    tel.inicializaGPS();

                    if (config.app.grap) {
                        tel.CreateCanvasGForce()
                        config.app.grap = false
                    }
                });

            }, function() {


                document.location = '#ayudaTelemetria'

            }, options);




        }  else {


            $('.wsContenido').empty();
            $('#home').css('display', 'none');
            $('.telemetria').css('display', 'none');
            $('.contenido').css('display', '');

            jash = jash.substr(1);


            $('html,body').scrollTop(0);

            var url = config.app.ulrws + window.apikey + "/" + config.app.versionWS + "/" + config.app.lengua + "/get.content." + jash + ".json/0";

            app.loader(true)


            $.ajax({
                dataType: "json",
                url: url,
                data: data,
                success: function(json) {

                
                    app.loader(false);

                    $('.wsContenido').html(utf8_decode(base64_decode(json.HTML)));


                    $(".desplegar").click(function() {
                        var id = $(this).attr('rel');
                        var num_divs = 15;

                        for (var i = 0; i < num_divs; i++) {
                            if (i == id) {
                                $('#div_' + i).css('display', '');
                            } else {

                                $('#div_' + i).css('display', 'none');
                            }

                        }

                    });

                    if (jash == 'waypoints') {
                  
                        $("#verfotoway").click(function() 
                        {
                            var img = $(this).attr("data-imagen");
                            var texto = $(this).attr("data-texto");
                            var cordenada = $(this).attr("data-cordenada");

                        });
                    }

                    if (jash == 'reproductor') {
                        $("#videoHTML5src").attr("src", data.app.get);
                    }

                    $(".share").click(function() {
                        var url = $(this).attr('rel');
                        app.share(url);
                    });

                    $(".tiempoLoad").click(function() {

                        var lugar = $(this).attr('rel');
                        console.log(lugar)
                        if(lugar==undefined){

                            config.coord.lugar = "Barcelona";

                        }else
                        {
                            config.coord.lugar = lugar;
                        }
                        
                        app.loader(true);
                        setTimeout(app.jumpClima, 2000);


                    });

                },
                timeout: 2700
            }).fail(function(xhr, status) {
                if (status == "timeout") {
                   
                    app.loader(false);
                
                    messg.info('It seems that the internet connection is failing or is weak , try again.', 3500);
                    window.location.hash = "#home";
                }
            });


        }

    };

    $(".contenido").on("click", function() {

        $('#menu').css('display', 'none');

    });

    $(".menuBAR").on("click", function() {

        var rel = $(this).attr('rel');
        window.location.hash = rel;

        $(this).addClass('menuInteriorActivo');
    });

    $("#js-hamburger").on("click", function() {

        if (window.menuEstado) {
            $('#menu').css('display', 'none');
            window.menuEstado = false;
       
        } else {
            $('#menu').css('display', '');
            window.menuEstado = true;
       
        }
    });

    $(".BtonLoginSumit").click(function() {
        app.login();
        $(".BtonLogin").unbind();
        $(".BtonLoginSumit").unbind();

    });




    function btnHome2() {

        $('.BtonInvitado').css('display', 'none');
        $('.BtonLogin').css('display', 'none');
        $('.fromlogin').css('display', '');
        $(".BtonLogin").unbind();
        $(".BtonInvitado").unbind();
        $('.texloading').css('display', 'none');
    }

    $(".BtonLogin").click(function() {
        btnHome2()

    });

    $(".videowelcome").click(function() {
        app.playerVideo("http://seatexperience.net/backoffice/videos/"+window.videoHome+"_movil.mp4");
     });


    function MostrarBotones() {
        $('#BtonLogin').css('display', '');
        $('#BtonInvitado').css('display', '');
    }

    function resetLoginText() {
        $('.BtonLoginSumit').html('Intentar de nuevo');
    }